<?php
class Regis extends Controller {
    public function index() {
       $this->view("regis/index");
    }

    public function regist(){
        if($this->model('Auth_model')->regis($_POST) > 0){
            header("Location: ".BASE_URL."/login");
        }
    }
}
?>