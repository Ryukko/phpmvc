<?php
   class Home extends Controller {
       public function index() {
           // echo "Home/index";
           $data["judul"] = "Home";
           $data["nama"] = "Restu";
           $this->view("templates/header", $data);
           $this->view("home/index", $data);
           $this->view("templates/footer");
       }

       public function about() {
        $data["judul"] = "Home";
        $data["nama"] = "Restu";
        $this->view("templates/header", $data);
        $this->view("home/about", $data);
        $this->view("templates/footer");
        }
   }
?>