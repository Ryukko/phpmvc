<?php
class User extends Controller {
    public function index() {
       $data["judul"] = "User";
       $data["nama"] = $this->model("User_model")->tampilSemua();
       $this->view("templates/header", $data);
       $this->view("user/index");
       $this->view("templates/footer");
    }
    public function profile($nama = "Restu Pranata", $pekerjaan = "Joki Genshin Impact") {
       $data["judul"] = "User";
       $data["nama"] = $nama;
       $data["pekerjaan"] = $pekerjaan;
       $this->view("templates/header", $data);
       $this->view("user/profile", $data);
       $this->view("templates/footer");
    }
}
