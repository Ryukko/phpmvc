<?php
   class Blog extends Controller {
    public function index()
    {
        $data['blog'] = $this->model('Blog_model')->getAllBlogAndUser();
        $data['user'] = $this->model('User_model')->tampilSemua();
        $data['judul'] = 'Blog';

        $this->view('Templates/header', $data);
        $this->view('Blog/index', $data);
        $this->view('Templates/footer');
    }

    public function detail($id)
    {
        $data['title'] = 'Blog Detail';
        $data['blog'] = $this->model('Blog_model')->getBlogAndUserById($id);

        $this->view('Templates/header', $data);
        $this->view('Blog/detail', $data);
        $this->view('Templates/footer');
    }

    public function delete($id)
    {
        if($this->model('Blog_model')->delete($id) > 0)
        {
            header('Location:'.BASE_URL.'/blog');
            exit;
        }
    }

    public function tambah()
    {
        $penulisId = $_POST['idUser'];
        $blogData = [
            'judul' => $_POST['judul'],
            'tulisan' => $_POST['tulisan'],
            'idUser' => $penulisId,
        ];

        if($this->model('Blog_model')->tambahBlog($blogData) > 0)
        {
            header('Location:{BASEURL}/blog');
            exit;
        }
        else 
        {
            header('Location:{BASEURL}/blog');
            exit;
        }
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Blog';

        $data['blog'] = $this->model('Blog_model')->getBlogById($id);

        $this->view('Templates/header', $data);
        $this->view('Blog/edit', $data);
        $this->view('Templates/footer');
    }

    public function update($id)
    {
        $blogData = [
            'judul' => $_POST['judul'],
            'tulisan' => $_POST['tulisan'],
            'idUser' => $_POST['idUser'],
        ];

        if($this->model('Blog_model')->updateBlog($blogData, $id) > 0)
        {
            header('Location:'.BASE_URL.' /blog');
            exit;
        }
        
    }
   }
?>