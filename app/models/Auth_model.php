<?php  
class Auth_model {
    private $table = 'users';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function regis(){
        $pass =  md5(MD5_SALT.$_POST['pass']);
        $this->db->query("INSERT INTO {$this->table} VALUES (null, :Nama, :password)");
        $this->db->bind('Nama', $_POST['nama']);
        $this->db->bind('password',$pass);
        $this->db->execute();
        return 1;
    }

    public function login($data){
        $this->db->query('SELECT * FROM users WHERE nama=:nama');
        $this->db->bind('nama', $data['nama']);
        $this->db->execute();
        if ($this->db->rowCount()> 0) {
            $row = $this->db->resultSingle();
            $pass = md5(MD5_SALT.$data['pass']);
            if ($pass == $row["password"]) {
                return true;
            }
        }
    }

}
?>