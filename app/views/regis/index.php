<!DOCTYPE html>
<html lang="en">

 <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Halaman Register</title>
   <link rel="stylesheet" href="<?= BASE_URL ?>/css/bootstrap.css">
 </head>
 <body>
    
     <div class="container mt-3">
         <h2>REGISTER FORM</h2>
        <form method="POST" action="<?= BASE_URL ?>/regis/regist">
        <div class="mb-3">
          <label for="nama" class="form-label">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
          <label for="pass" class="form-label">Password</label>
          <input type="password" class="form-control" id="pass" name="pass">
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      <div class="mt-3">
        <span>Already Have an Account?  <a href="<?= BASE_URL ?>/login">Log in</a></span>
      </div>
    </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1cl HTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="<?= BASE_URL; ?>/js/bootstrap.js"></script>
 </body>

</html>